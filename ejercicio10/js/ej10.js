/*función que se encarga de lanzar el dado*/
function lanzar_dado() {
    /*número random para elegir el dado*/
    let num = Math.floor((Math.random()*6)+1);

    /*visualización de lanzamiento
    para cada resultado se cambia la respectiva foto*/
    //window.alert("El número resultado es " + num);
    switch(num){
        case 1:
        document.getElementById('dado').src = "Fotos/1.png";
        break;
        case 2:
        document.getElementById('dado').src = "Fotos/2.png";
        break;
        case 3:
        document.getElementById('dado').src = "Fotos/3.png";
        break;
        case 4:
        document.getElementById('dado').src = "Fotos/4.png";
        break;
        case 5:
        document.getElementById('dado').src = "Fotos/5.png";
        break;
        case 6:
        document.getElementById('dado').src = "Fotos/6.png";
        break;
    }
    document.getElementById('lanzar').innerHTML="Lanzar dado otra vez";
    document.getElementById('description').innerHTML="Usted obtuvo el número " + num + "!";
}
