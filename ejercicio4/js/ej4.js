/*función que encuentra el mayor de 3*/
function comparacion() {
    /*variables*/
    let num1 = prompt("Ingrese primer número: ");
    let num2 = prompt("Ingrese segundo número: ");
    let num3 = prompt("Ingrese tercer número: ");

    /*comparación*/
    /*primer número mayor*/
    if ((num1>num2) && (num1>num3)) {
        window.alert("El número mayor es: " + num1);
    }
    /*se evalúa el segundo si el primero no es el mayor*/
    else if (num2>num3) {
        window.alert("El número mayor es: " + num2);
    }
    /*se evalúa si es el tercer número el mayor*/
    else if (num3>num2){
        window.alert("El número mayor es: " + num3);
    }
    /*la última condición fue para descartar que sean los 3 el mismo número*/
    else{
      window.alert("Ingresó el mismo número 3 veces.")
    }
}
/*se llama a la función*/
comparacion();
