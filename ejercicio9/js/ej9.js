/*el 30 de abril de 2022 1 CLP = 855*/

/*función que recibe dólares y los convierte en pesos*/
function cambio_a_clp(){
    var ToUSD = document.getElementById("dolar").value;
    let cambioA = ToUSD*855;

    /*si no ingresa nada*/
    if(ToUSD == ""){
        window.alert("Por favor rellene el campo");
    }
    /*si no ingresa números*/
    else if(isNaN(ToUSD)){
        window.alert("Ingrese números, no se pueden convertir caracteres");
    }
    else{
        document.getElementById('cambio_usd').value=parseFloat(cambioA.toFixed(5));
    }
}

/*función que recibe pesos chilenos y los convierte en dólares*/
function cambio_a_usd(){
    var ToCLP = document.getElementById("pesos").value;
    let cambioB = ToCLP/855;

    /*si no ingresa nada*/
    if(ToCLP == ""){
        window.alert("Por favor rellene el campo");
    }
    /*si ingresa caracteres*/
    else if(isNaN(ToCLP)){
        window.alert("Ingrese números, no se pueden convertir caracteres");
    }
    else{
        document.getElementById('cambio_clp').value=parseFloat(cambioB.toFixed(5));
    }
}
