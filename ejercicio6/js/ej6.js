/*función que cuenta las A*/
function cuantas_A() {
    /*ingreso de la frase*/
    let frase = prompt(" Ingrese una frase:");

    /*se cuenta con la función match cuantas A encuentra en la frase
    utilizando en primer lugar toLowerCase para tener todas las letras A en minúscula*/
    let a = frase.toLowerCase().match(/a/g);

    /* se cuentan las A encontradas*/
    let contador = 0;
    for(i in a){
      contador++;
    }
    alert("Cantidad de a: " + contador)
}
/*se llama a la función*/
cuantas_A();
