/*arrays para cambiar el contenido de la página*/
const fotos = ["1", "2", "3", "4", "5"];
const descripciones = ["Delilah Bard, Shades of Magic",
												"Julian Blackthorn, The Dark Artifices",
												"Isabelle Lightwood, The Mortal Instruments",
												"Adam y Olive, The Love Hypothesis",
												"Evelyn Hugo, The Seven Husbands of Evelyn Hugo"];

/*para dar la ruta*/
ruta = "Fotos/";
extension = ".jpg";

/*contador de la imagen en 1*/
var i = 1;

/*función para ir a la imagen anterior*/
function anterior(){
	console.log(i);
	/*si la imagen es la primera entonces se visualiza la quinta
	si no es la primera entonces simplemente se retrocede a la imagen anterior*/
	switch (i) {
		case 1:
		i=5;
		break;
		default:
		i--;
		break;
	}
	/*se cambia la imagen (i-1 ya que usa índices del 0 al 4) y su descripción*/
	document.getElementById("galeria").src = ruta + fotos[i-1] + extension;;
	document.getElementById("descripcion").innerHTML = descripciones[i-1];
}

/*función para ir a la imagen siguiente*/
function siguiente(){
	console.log(i);
	/*si la imagen es la última entonces se va a la primera
	si no lo es entonces solo se sigue adelante*/
	switch (i) {
		case 5:
		i=1;
		break;
		default:
		i++;
		break;
	}
	/*se cambia la imagen (i-1 ya que usa índices del 0 al 4) y su descripción*/
	document.getElementById("galeria").src = ruta + fotos[i-1] + extension;;
	document.getElementById("descripcion").innerHTML = descripciones[i-1];
}
