/*función que encuentra vocales*/
function cuantas_vocales() {
    /*variables de vocales y lo que será respuesta a las encontradas*/
    let respuesta = "¿Cuántas veces aparecen las vocales?";
    const vocales = ["a", "e", "i", "o", "u"];
    const contador = [0, 0, 0, 0, 0];

    /*pide el ingreso de una frase*/
    let frase = prompt("Ingrese una Frase:");
    respuesta = respuesta + "\n" + frase;

    /*contadores para encontrar recorrer frase y vocales*/
    let i = 0;
    let j = 0;

    while(i<vocales.length){ /*recorre vocales*/
      while(j<frase.length){ /*recorre frase*/
        /*si la letra presente es la vocal buscada se suma a su contador*/
        if(frase[j].toLowerCase()==vocales[i]){
          contador[i]++;
        }
        j++;
      }
      /*se agrega la cantidad de vocales encontrada*/
      respuesta = respuesta + "\n" + vocales[i].toUpperCase() + ": " + contador[i];
      j=0; /*se reinicia contador de frase para volver a recorrerla*/
      i++;
    }

    /*se muestra la respuesta*/
    window.alert(respuesta);
}

/*llamado a la función*/
cuantas_vocales()
